$(document).ready(function(){
    
    //back to top
    var btt = $('.back_to_top');
    
    btt.on('click', function(e) {
       $('html, body').animate({
           scrollTop: 0
       }, 500);
        
        e.preventDefault();
    });
    
    $(window).on('scroll',function(){
        var self = $(this),
            height = self.height(),
            top = self.scrollTop();
        
        if(top > height) {
            if(!btt.is(':visible')) {
                btt.show();
            }
        } else {
            btt.hide();
        }
    })
    
    //parallax js
    $.stellar();
    
    // wow js
    new WOW().init();
    
    //Nivo slider js
     $('#slider').nivoSlider();
	
	//slicknav mobile menu
	$('.nav').slicknav(); 
	
	//preloader js
	 $("body").addClass("perloader_active");
	
	
	// start fade menu js
	$("ul.nav ul.sub-menu").css('opacity',0.0);
	$("ul.nav li").hover(function() {
	$(this).find(".sub-menu").stop().animate({opacity:1});
	},
	function() {
	$(this).find(".sub-menu").stop().animate({opacity:0});
	});	
	//end fade menu js
	
	// start submenu
	$("ul.sub-menu ul.s-menu").css('opacity',0.0);
	$("ul.sub-menu li ").hover(function() {
	$(this).find(".s-menu").stop().animate({opacity:1});
	},
	function() {
	$(this).find(".s-menu").stop().animate({opacity:.0});
	});
	// start submenu	
			
	
});

// preloader js 
 $(window).load(function(){
     
     
     
     
               
		$("#preloader").fadeOut();
		$(".spiner_preloader").delay(350).fadeOut();
		$("body").removeClass("preloader_active");
               
});


